# PGP Starter Guide

## Lage nøkkel

### RSA

### ECC

```
$ gpg --expert --full-gen-key
gpg (GnuPG) 2.2.19; Copyright (C) 2019 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
   (9) ECC and ECC
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (13) Existing key
  (14) Existing key from card
Your selection? 11
```

```
Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate
Current allowed actions: Sign Certify

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? s
```

```
Possible actions for a ECDSA/EdDSA key: Sign Certify Authenticate
Current allowed actions: Certify

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? q
```

```
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Your selection? 1
```

```
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
```

```
Key expires at Fri Mar 18 18:50:19 2022 CET
Is this correct? (y/N) u
```

```
GnuPG needs to construct a user ID to identify your key.

Real name: Test User
Email address: text@example.com
Comment:
You selected this USER-ID:
    "Test User <text@example.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
```

```
┌──────────────────────────────────────────────────────┐
│ Please enter the passphrase to                       │
│ protect your new key                                 │
│                                                      │
│ Passphrase: ________________________________________ │
│                                                      │
│       <OK>                              <Cancel>     │
└──────────────────────────────────────────────────────┘
```

```
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key C56FC5B11DAACEE0 marked as ultimately trusted
gpg: revocation certificate stored as '/Users/$USER/.gnupg/openpgp-revocs.d/24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0.rev'
public and secret key created and signed.

pub   ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
uid                      Test User <text@example.com>
```

Da har vi laget en nøkkel som kun kan lage nye subkeys eller revocation certificate.
Denne inneholder kun hovenøkkeln og har i tillegg opprettet et revocations certificate.
Den siste er hvis nøkkelen blir komprimitert og du må nuke den (gjøre den ikke gyldig).

```
$ gpg --list-keys
gpg: checking the trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   2  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 2u
gpg: next trustdb check due at 2022-03-18
/Users/$USER/TempPGP/pubring.kbx
-----------------------------
pub   ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
uid           [ultimate] Test User <text@example.com>
```

## Subkeys & master key

Per nå har vi ingen subkeys.  Og som teksten viser på linjen som starter på pub, finner vi bare `[C]`.
Dette betyr at nå kan nøkkel kun lage nye subkeys og ikke gjøre signering autentisering, kryptering.

```
$ gpg --list-keys --with-keygrip
/Users/$USER/TempPGP/pubring.kbx
-----------------------------

pub   ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
      Keygrip = 5EF8C62AA8B525F51331053FF083C8A0AEB45D98
uid           [ultimate] Test User <text@example.com>
```

(keygrip viser selve navnet på nøkkelen. Disse ligger under ` ~/.gnupg/private-keys-v1.d/`.)

### Subkeys

```
$ gpg --homedir TempPGP --expert --edit-key 24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
gpg (GnuPG) 2.2.19; Copyright (C) 2019 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Secret key is available.

sec  ed25519/C56FC5B11DAACEE0
     created: 2020-03-18  expires: 2022-03-18  usage: C
     trust: ultimate      validity: ultimate
[ultimate] (1). Test User <text@example.com>

gpg>
```

#### Signing

```
gpg> addkey
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 10
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
Key expires at Fri Mar 18 19:20:03 2022 CET
Is this correct? (y/N) y
Really create? (y/N) y

┌────────────────────────────────────────────────────────────────┐
│ Please enter the passphrase to unlock the OpenPGP secret key:  │
│ "Test User <text@example.com>"                                 │
│ 256-bit EDDSA key, ID C56FC5B11DAACEE0,                        │
│ created 2020-03-18.                                            │
│                                                                │
│                                                                │
│ Passphrase: ************______________________________________ │
│                                                                │
│         <OK>                                    <Cancel>       │
└────────────────────────────────────────────────────────────────┘

We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  ed25519/C56FC5B11DAACEE0
     created: 2020-03-18  expires: 2022-03-18  usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/BF97BA8909EF80DD
     created: 2020-03-18  expires: 2022-03-18  usage: S
[ultimate] (1). Test User <text@example.com>
```


#### Kryptering

```
gpg> addkey
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 12
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
Key expires at Fri Mar 18 19:22:51 2022 CET
Is this correct? (y/N) y
Really create? (y/N) y
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  ed25519/C56FC5B11DAACEE0
     created: 2020-03-18  expires: 2022-03-18  usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/BF97BA8909EF80DD
     created: 2020-03-18  expires: 2022-03-18  usage: S
ssb  cv25519/684C0A834601E377
     created: 2020-03-18  expires: 2022-03-18  usage: E
[ultimate] (1). Test User <text@example.com>
```

#### Autentisering

Denne krever litt flere steg.  Må manuelt velge seg frem til ønsket nøkkel-type.

```
gpg> addkey
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 11

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions: Sign

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? s

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions:

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? a

Possible actions for a ECDSA/EdDSA key: Sign Authenticate
Current allowed actions: Authenticate

   (S) Toggle the sign capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? q
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
Key expires at Fri Mar 18 19:24:15 2022 CET
Is this correct? (y/N) y
Really create? (y/N) y
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  ed25519/C56FC5B11DAACEE0
     created: 2020-03-18  expires: 2022-03-18  usage: C
     trust: ultimate      validity: ultimate
ssb  ed25519/BF97BA8909EF80DD
     created: 2020-03-18  expires: 2022-03-18  usage: S
ssb  cv25519/684C0A834601E377
     created: 2020-03-18  expires: 2022-03-18  usage: E
ssb  ed25519/A23997E526C18DBF
     created: 2020-03-18  expires: 2022-03-18  usage: A
[ultimate] (1). Test User <text@example.com>
```

Når alt er ferdig er det å trykke `q` og `y` for å lagre endringen.

```
gpg> q
Save changes? (y/N) y
```

```
$ gpg  --list-secret-keys
/Users/$USER/.gnupg/pubring.kbx
-----------------------------
pub   ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
uid           [ultimate] Test User <text@example.com>
sub   ed25519 2020-03-18 [S] [expires: 2022-03-18]
sub   cv25519 2020-03-18 [E] [expires: 2022-03-18]
sub   ed25519 2020-03-18 [A] [expires: 2022-03-18]
```

Da er alle nøklene opprettet og vi nå ser at hver nøkkel har sin rolle (bokstaven inne i klammeparantes). Viktig her å merke seg kodeordene foran hver nøkkel.  Disse forteller hvilke type de er og gir oss litt hjelp med å hvis oss hvilke nøkler vi jobber på.

### Fjerne masterkeys & revoke

Som vist over med flagget `--with-keygrip` har vi nå IDen på selve nøkkelen vi må fjerne fra ` ~/.gnupg/private-keys-v1.d/`. **NB!** Denne skal lagres til senere bruk. **NB!** Ta godt vare på denne filen.

```
$ gpg --list-secret-keys --with-keygr
/Users/$USER/.gnupg/pubring.kbx
-----------------------------
sec   ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
      Keygrip = 5EF8C62AA8B525F51331053FF083C8A0AEB45D98
uid           [ultimate] Test User <text@example.com>
ssb   ed25519 2020-03-18 [S] [expires: 2022-03-18]
      Keygrip = 7D906C7B155291ECCC0BD8FF6E98E6A13D43B3E4
ssb   cv25519 2020-03-18 [E] [expires: 2022-03-18]
      Keygrip = CC4426C3C6335DAA9AB6CBD7F29CB336DE9C550D
ssb   ed25519 2020-03-18 [A] [expires: 2022-03-18]
      Keygrip = 9D44C777C377EDB70C623A471AEEA896C2AF7B7A
```

Over ser vi ordet `sec` foran og ikke `pub`.  Dette indikerer at vi nå jobber på/ser på den private nøkkelen.  Men den er ikke i en tilstand vi ønsker, da den nå kan lett benyttes til å lage nye nøkler og evt. revoke eksisterende nøkler.  Til og med seg selv.

Vi dermed disse kommandoer for å fjerne hovednøkkelen fra nøkkelringen:

```
mv ~/.gnupg/private-keys-v1.d/5EF8C62AA8B525F51331053FF083C8A0AEB45D98 SECURE_USB
mv ~/.gnupg/openpgp-revocs.d/24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0.rev SECURE_USB
```

**NB!** Aldri importer revoke-sertifaktet uten at du faktisk har tenkt å ugyldiggjøre nøkkelen.

Etter disse er utført, skal nå sitte igjen med følgende:

```
$ gpg --list-secret-keys --with-keygrip
/Users/$USER/.gnupg/pubring.kbx
-----------------------------
sec#  ed25519 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
      Keygrip = 5EF8C62AA8B525F51331053FF083C8A0AEB45D98
uid           [ultimate] Test User <text@example.com>
ssb   ed25519 2020-03-18 [S] [expires: 2022-03-18]
      Keygrip = 7D906C7B155291ECCC0BD8FF6E98E6A13D43B3E4
ssb   cv25519 2020-03-18 [E] [expires: 2022-03-18]
      Keygrip = CC4426C3C6335DAA9AB6CBD7F29CB336DE9C550D
ssb   ed25519 2020-03-18 [A] [expires: 2022-03-18]
      Keygrip = 9D44C777C377EDB70C623A471AEEA896C2AF7B7A
```

**NB!** Legg merke til `sec#`.  Dette er den ønskede tilstanden vi ønsker.  Nå kan nøkkelen kun gjøre de opperasjonene som vi har gitt med subkeys.

## Integrering

Etter vi har laget oss nøkkelen ønsker vi å ta i bruk.  Unnder gis det noen eksempler på hvor det er naturlig å benytte den nye nøkkelen.

### Git

```
 gpg --list-keys --keyid-format LONG
/Users/$USER/.gnupg/pubring.kbx
-----------------------------
pub   ed25519/C56FC5B11DAACEE0 2020-03-18 [C] [expires: 2022-03-18]
      24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0
uid                 [ultimate] Test User <text@example.com>
sub   ed25519/BF97BA8909EF80DD 2020-03-18 [S] [expires: 2022-03-18]
sub   cv25519/684C0A834601E377 2020-03-18 [E] [expires: 2022-03-18]
sub   ed25519/A23997E526C18DBF 2020-03-18 [A] [expires: 2022-03-18]
```

På linjen `pub` og bak `ed25519/` finner vi IDen vi ønsker å legge til `.gitconfig`.

```git
[user]
  name = Test User
  email = test@example.com
  signingkey = C56FC5B11DAACEE0
```

### SSH

Ved å benytte kun PGP-nøkkelen til SSH, slipper vi å lage nye SSH-nøkler for hver tjener.  Vi trenger da kun å dele den fra PGP-nøkkelen.

#### Environment

Vi må sette noen variables i terminalen.

```sh
export GPG_TTY=$(tty)  # Aktuelt for pinentry i terminal
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent  # Startes manuelt med denne linjen. Må være med!
```

#### Bytte til PGP key for SSH

`Keygrip` for nøkkelen med `[A]` rettighet skal vi legge til filen `~/.gnupg/sshcontrol`.

```
$ gpg --list-keys --with-keygrip
/Users/$USER/.gnupg/pubring.gpg
----------------------------
pub   rsa4096 2015-01-14 [SC] [expires: 2096-12-06]
      6F0F69DC7671B2E61E413BC4BE2567C43DEC9BA4
      Keygrip = EE0F2C667ECD432D6D7774F32EE71F35C9AFD359
uid           [ultimate] Birger J. Nordølum <contact@mindtooth.no>
sub   rsa4096 2015-01-14 [E] [expires: 2096-12-06]
      Keygrip = B4822EC75A2FA2AA9DJB9CA1B60D6E1729A9F954
sub   rsa4096 2015-02-22 [S] [expires: 2096-12-06]
      Keygrip = C94850343F36F7E21EB6SL901E5BA22BC53C8D11
sub   rsa4096 2017-12-05 [A] [expires: 2096-12-06]
  fb    Keygrip = F495B7B341620623EE26E6911B09FD33CD4E5FD4
```

```
$ cat ~/.gnupg/sshcontrol
F495B7B341620623EE26E6911B09FD33CD4E5FD4
```

Må merke at denne filen også kan inneholde eksisterende SSH-nøkler etter hvert som du legger til gamle nøkler (SSH).

---

For få å ta i bruk selve nøkkelen når du skal logge inn, må den eksporteres og legges til `.ssh/config`.

```
$ gpg --export-ssh-key 24A4252DE0FB8B44EC4AA057C56FC5B11DAACEE0 > .ssh/id_pgp
$ cat ~/.ssh/config
Host exm
HostName example.com
User root
IdentityFile ~/.ssh/id_pgp  # <- MERK!
```

**NB!** Dette er den samme nøkkelen du legger til i `autorized_hosts` på serveren, samt benytter som `IdentidyFile` i `.ssh/config`.
Dette er ikke vanlig og du vil oppleve feil hvis du ikke kjører `gpg-agent`.

```
ssh-copy-id -i ~/.ssh/id_pgp ssh.example.com
```

## Reference

### Help

```
quit        quit this menu
save        save and quit
help        show this help
fpr         show key fingerprint
grip        show the keygrip
list        list key and user IDs
uid         select user ID N
key         select subkey N
check       check signatures
sign        sign selected user IDs [* see below for related commands]
lsign       sign selected user IDs locally
tsign       sign selected user IDs with a trust signature
nrsign      sign selected user IDs with a non-revocable signature
adduid      add a user ID
addphoto    add a photo ID
deluid      delete selected user IDs
addkey      add a subkey
addcardkey  add a key to a smartcard
keytocard   move a key to a smartcard
bkuptocard  move a backup key to a smartcard
delkey      delete selected subkeys
addrevoker  add a revocation key
delsig      delete signatures from the selected user IDs
expire      change the expiration date for the key or selected subkeys
primary     flag the selected user ID as primary
pref        list preferences (expert)
showpref    list preferences (verbose)
setpref     set preference list for the selected user IDs
keyserver   set the preferred keyserver URL for the selected user IDs
notation    set a notation for the selected user IDs
passwd      change the passphrase
trust       change the ownertrust
revsig      revoke signatures on the selected user IDs
revuid      revoke selected user IDs
revkey      revoke key or selected subkeys
enable      enable key
disable     disable key
showphoto   show selected photo IDs
clean       compact unusable user IDs and remove unusable signatures from key
minimize    compact unusable user IDs and remove all signatures from key

* The 'sign' command may be prefixed with an 'l' for local signatures (lsign),
  a 't' for trust signatures (tsign), an 'nr' for non-revocable signatures
  (nrsign), or any combination thereof (ltsign, tnrsign, etc.).
```
